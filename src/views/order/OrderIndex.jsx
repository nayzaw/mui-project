import { useEffect, useState } from 'react';
import { Box, Container, Typography } from '@mui/material';
import Accordion from '@mui/material/Accordion';
import AccordionDetails from '@mui/material/AccordionDetails';
import AccordionSummary from '@mui/material/AccordionSummary';
import ExpandMoreIcon from '@mui/icons-material/ExpandMore';
import Summary from './Summary';
import OrderDetail from './OrderDetail';
import { useNavigate } from 'react-router-dom';


const OrderIndex = () => {
	const navigate = useNavigate();
	const [orderData] = useState(JSON.parse(localStorage.getItem('ORDER_DATA')) || [])
	const [user] = useState(JSON.parse(localStorage.getItem('CURRENT_USER')) || null)
	const [data, setData] = useState([]);
	const [expanded, setExpanded] = useState(false);


	useEffect(() => {
		if(user && orderData.length > 0){
			const result = orderData.filter(i => i.userID === user.id);

			if(result.length > 0){
				setData(result)
			}else{
				navigate('/empty-order');
			}
		}else{
			navigate('/empty-order');
		}
	}, [])

	const handleChange = (panel) => (event, isExpanded) => {
		setExpanded(isExpanded ? panel : false);
	};

	return(
		<Container maxWidth='md' disableGutters>
			{
				data.length > 0 &&
				data.map((i, index) => {
					return(
						<Box sx={{ mb: 1 }} key={index}>
							<Accordion expanded={expanded === `order${index}`} onChange={handleChange(`order${index}`)} className='cardShadow' key={index}>
								<AccordionSummary
									expandIcon={<ExpandMoreIcon />}
									aria-controls={`order${index}-content`}
									id={`order${index}-header`}
								>
									<Summary i={i} />
								</AccordionSummary>
								<AccordionDetails>
									<OrderDetail products={i.products} />
								</AccordionDetails>
							</Accordion>
						</Box>
					)
				})
			}
		</Container>
	)
}

export default OrderIndex