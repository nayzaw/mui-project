import React from 'react';
import { Grid, Typography } from '@mui/material';


const Summary = ({ i }) => {
	return(
		<Grid container>
			<Grid container mb={1}>
				<Grid item md={12}>
					<Typography variant="button" display="block" gutterBottom color='primary' sx={{ fontWeight: 'bold' }}>
						Order #{i.orderID}
					</Typography>
				</Grid>
			</Grid>
			
			<Grid container>
				<Grid item xs={4} md={2.5}>
					<Typography variant="subtitle2" component="div" gutterBottom>
						Products
					</Typography>
				</Grid>
				<Grid item xs={8} md={4}>
					<Typography variant="subtitle2" component="div" gutterBottom>
						{i.products.length}
					</Typography>
				</Grid>
			</Grid>

			<Grid container>
				<Grid item xs={4} md={2.5}>
					<Typography variant="subtitle2" component="div" gutterBottom>
						Placed on
					</Typography>
				</Grid>
				<Grid item xs={8} md={4}>
					<Typography variant="subtitle2" component="div" gutterBottom>
						{i.placedOn}
					</Typography>
				</Grid>
			</Grid>

			<Grid container>
				<Grid item xs={4} md={2.5}>
					<Typography variant="subtitle2" component="div" gutterBottom>
						Total Amount
					</Typography>
				</Grid>
				<Grid item xs={8} md={4}>
					<Typography variant="subtitle2" component="div" gutterBottom>
						${i.total}
					</Typography>
				</Grid>
			</Grid>
		</Grid>
	)
}

export default Summary