import React, { Fragment } from 'react';
import { Box, Divider, Grid, Typography } from '@mui/material';

const OrderDetail = ({ products }) => {

	const s = {
		img: { display: 'flex', alignItems: 'center' },
		title: { fontSize: '0.9rem', fontWeight: 'bold' },
		qtyAction: { display: 'flex', alignItems: 'center' },
		size: {
			width: '100%',
			maxHeight: 100,
			objectFit: 'contain'
		}
	}

	return(
		<Grid item md={12}>
			{
				products.map((ii, index2) => {
					return (
						<Fragment key={index2}>
							<Grid container mt={2} mb={1.5} >
								<Grid item md={2} xs={3} sx={s.img}>
									<Box
										className="selector img"
										component="img"
										sx={s.size}
										alt={ii.title}
										src={ii.image}
									/>
								</Grid>

								<Grid item md={10} xs={9} sx={{ display: 'flex', flexWrap: 'wrap' }}>
									<Box pl={3}>
										<Typography variant="h6" gutterBottom component="div" sx={s.title}>
											{ii.title}
										</Typography>
										<Typography variant="caption" display="block" gutterBottom>
											{ii.category}
										</Typography>
									</Box>
									<Grid container sx={{ alignSelf: 'flex-end', pl: 3 }}>
										<Grid item xs={6} md={3} sx={s.qtyAction}>
											<Typography variant="body2">
												Quantity: {ii.quantity}
											</Typography>
										</Grid>
										<Grid item xs={6} md={3} sx={s.qtyAction}>
											<Typography variant="body2">
												${ii.price}
											</Typography>
										</Grid>
									</Grid>
								</Grid>
							</Grid>
							{index2 < products.length - 1 && <Divider />}
						</Fragment>
					)
				})
			}
		</Grid>
	)
}

export default OrderDetail