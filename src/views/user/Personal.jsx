import { useSelector } from "react-redux";
import { Box, Button, Card, CardContent, CardHeader, Grid, CardActions } from "@mui/material";
import { LogoutIndex } from "../auth/logout/LogoutIndex";
import TextField from '@mui/material/TextField';
import Avatar from '@mui/material/Avatar';
import Skeleton from '@mui/material/Skeleton';
import { Fragment } from 'react';

const MySkeleton = () => <Skeleton variant="text" width="65%" height={30} sx={{ m: 2 }} />

export const Personal = () => {

	const user = useSelector(state => state.user.currentUser);
	const status = useSelector(state => state.user.status);

	return(
		<Fragment>
			<Card className="cardShadow" sx={{ pb: 2 }}>
				{
					status === 'pending'
					?
					<CardHeader component={MySkeleton} />
					:
					<CardHeader title="Personal Information" />
				}
				<CardContent>
					<Box sx={{ display: 'flex', justifyContent: 'center', mb: 3 }}>
						{
							status === 'pending' &&
							<Skeleton variant="circular">
								<Avatar sx={{ width: 100, height: 100 }} />
							</Skeleton>
						}
						{
							(status === 'finished' && user !== null) &&
							<Avatar
								alt={`${user.name.firstname} ${user.name.lastname}`}
								src="/static/images/avatar/1.jpg"
								sx={{ width: 100, height: 100, display: 'flex', justifyContent: 'center' }}
							/>
						}
					</Box>

					<Grid container spacing={2.5}>
						<Grid item xs={6} md={6}>
							{
								status === 'pending' &&
								<Fragment>
									<Skeleton variant="text" width="40%" height={10} />
									<Skeleton variant="text" height={10} />
								</Fragment>
							}
							{
								(status === 'finished' && user !== null) &&
								<TextField
									fullWidth
									label="First Name"
									defaultValue={user.name.firstname}
									InputProps={{
										readOnly: true,
									}}
									variant="standard"
								/>
							}
						</Grid>
						<Grid item xs={6} md={6}>
							{
								status === 'pending' &&
								<Fragment>
									<Skeleton variant="text" width="40%" height={10} />
									<Skeleton variant="text" height={10} />
								</Fragment>
							}
							{
								(status === 'finished' && user !== null) &&
								<TextField
									fullWidth
									label="Last Name"
									defaultValue={user.name.lastname}
									InputProps={{
										readOnly: true,
									}}
									variant="standard"
								/>
							}
						</Grid>

						<Grid item xs={12} md={6}>
							{
								status === 'pending' &&
								<Fragment>
									<Skeleton variant="text" width="40%" height={10} />
									<Skeleton variant="text" height={10} />
								</Fragment>
							}
							{
								(status === 'finished' && user !== null) &&
								<TextField
									fullWidth
									label="Username"
									defaultValue={user.username}
									InputProps={{
										readOnly: true,
									}}
									variant="standard"
								/>
							}
						</Grid>

						<Grid item xs={12} md={6}>
							{
								status === 'pending' &&
								<Fragment>
									<Skeleton variant="text" width="20%" height={10} />
									<Skeleton variant="text" height={10} />
								</Fragment>
							}
							{
								(status === 'finished' && user !== null) &&
								<TextField
									fullWidth
									label="Email"
									defaultValue={user.email}
									InputProps={{
										readOnly: true,
									}}
									variant="standard"
								/>
							}
						</Grid>

						<Grid item xs={12} md={12}>
							{
								status === 'pending' &&
								<Fragment>
									<Skeleton variant="text" width="20%" height={10} />
									<Skeleton variant="text" height={10} />
								</Fragment>
							}
							{
								(status === 'finished' && user !== null) &&
								<TextField
									fullWidth
									label="Phone"
									defaultValue={user.phone}
									InputProps={{
										readOnly: true,
									}}
									variant="standard"
								/>
							}
						</Grid>
					</Grid>
				</CardContent>
				
				{
					status === 'pending' &&
					<Skeleton variant="text" height={60} sx={{ mx: 1, my: 1, borderRadius: 6, display: { xs: 'none', md: 'flex' } }} />
				}
				{
					(status === 'finished' && user !== null) &&
					<CardActions sx={{ my: 1, mx: 1, display: { xs: 'none', md: 'flex' } }}>
						<Button
							onClick={LogoutIndex}
							fullWidth
							variant="outlined"
							color="error"
							sx={{
								borderRadius: 10,
							}}
						>
							Logout
						</Button>
					</CardActions>
				}
			</Card>
		</Fragment>
	)
}