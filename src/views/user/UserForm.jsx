import { Grid, Container } from "@mui/material"
import { Personal } from "./Personal";
import { Contact } from "./Contact";

export const UserForm = () => {

	return(
		<Container maxWidth="md" disableGutters>
			<Grid container spacing={2}>
				<Grid item xs={12} md={5}>
					<Personal />
				</Grid>
				<Grid item xs={12} md={7}>
					<Contact />
				</Grid>
			</Grid>
		</Container>
	)
}