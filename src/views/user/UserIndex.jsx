import React, { Fragment } from 'react';
import { useDispatch, useSelector } from "react-redux";
import { fetchSingleUser } from '../../thunks/user/fetchSingleUser';
import { Spinner } from '../../my-common/Spinner';
import { UserForm } from './UserForm';
import { useNavigate } from 'react-router-dom';
import { Error } from '../../my-common/Error';
import { Button } from '@mui/material';
import { Box } from '@mui/system';

const UserIndex = () => {

	const error = useSelector(state => state.user.error)
	const status = useSelector(state => state.user.status)
	const navigate = useNavigate();
	const [user] = React.useState(JSON.parse(localStorage.getItem('CURRENT_USER')) || null);
	const dispatch = useDispatch();
	let content;


	React.useEffect(() => {
		if(user){
			dispatch(fetchSingleUser(user.id));
		}else{
			navigate('/');
		}
	}, [])
	

	// if(status === 'pending'){
	// 	content = <Spinner />
	// }
	// else if(status === 'finished'){
	// 	content = <UserForm />
	// }
	// else if(status === 'failed'){
	// 	content = <Error error={error} />
	// }


	if (status === 'finished' || status === 'pending'){
		content = <UserForm />
	}
	else if(status === 'failed'){
		content = <Fragment>
			<Error error={error} />
			<Box sx={{ display: 'flex', minHeight: '80vh', alignItems: 'center', justifyContent: 'center' }}>
				<Button variant='outlined' color='error' onClick={() => window.location.reload()}>Please Try Again</Button>
			</Box>
		</Fragment>
	}



	return(
		<React.Fragment>
			{content}
		</React.Fragment>
	)
}

export default UserIndex