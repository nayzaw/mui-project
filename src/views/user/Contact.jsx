import { useSelector } from "react-redux";
import {
	Card, CardContent, CardHeader, Grid, Button, CardActions } from "@mui/material"
import { Fragment } from "react";
import TextField from '@mui/material/TextField';
import Skeleton from '@mui/material/Skeleton';
import { LogoutIndex } from "../auth/logout/LogoutIndex";


const MySkeleton = () => <Skeleton variant="text" width="50%" height={30} sx={{ m: 2 }} />

export const Contact = () => {

	const user = useSelector(state => state.user.currentUser);
	const status = useSelector(state => state.user.status);

	return(
		<Card className="cardShadow" sx={{ pb: 2 }}>
			{
				status === 'pending'
				?
				<CardHeader component={MySkeleton} />
				:
				<CardHeader title="Contact Information" />
			}
			
			<CardContent>
				<Grid container spacing={2.5}>
					<Grid item xs={12} md={6}>
						{
							status === 'pending' &&
							<Fragment>
								<Skeleton variant="text" width="15%" height={10} />
								<Skeleton variant="text" height={10} />
							</Fragment>
						}
						{
							(status === 'finished' && user !== null) &&
							<TextField
								fullWidth
								label="City"
								defaultValue={user.address.city}
								InputProps={{
									readOnly: true,
								}}
								variant="standard"
							/>
						}
					</Grid>

					<Grid item xs={12} md={6}>
						{
							status === 'pending' &&
							<Fragment>
								<Skeleton variant="text" width="17%" height={10} />
								<Skeleton variant="text" height={10} />
							</Fragment>
						}
						{
							(status === 'finished' && user !== null) &&
							<TextField
								fullWidth
								label="Street"
								defaultValue={user.address.street}
								InputProps={{
									readOnly: true,
								}}
								variant="standard"
							/>
						}
					</Grid>

					<Grid item xs={6} md={6}>
						{
							status === 'pending' &&
							<Fragment>
								<Skeleton variant="text" width="21%" height={10} />
								<Skeleton variant="text" height={10} />
							</Fragment>
						}
						{
							(status === 'finished' && user !== null) &&
							<TextField
								fullWidth
								label="Zip Code"
								defaultValue={user.address.zipcode}
								InputProps={{
									readOnly: true,
								}}
								variant="standard"
							/>
						}
					</Grid>

					<Grid item xs={6} md={6}>
						{
							status === 'pending' &&
							<Fragment>
								<Skeleton variant="text" width="22%" height={10} />
								<Skeleton variant="text" height={10} />
							</Fragment>
						}
						{
							(status === 'finished' && user !== null) &&
							<TextField
								fullWidth
								label="Number"
								defaultValue={user.address.number}
								InputProps={{
									readOnly: true,
								}}
								variant="standard"
							/>
						}
					</Grid>

					<Grid item xs={6} md={6}>
						{
							status === 'pending' &&
							<Fragment>
								<Skeleton variant="text" width="23%" height={10} />
								<Skeleton variant="text" height={10} />
							</Fragment>
						}
						{
							(status === 'finished' && user !== null) &&
							<TextField
								fullWidth
								label="Latitude"
								defaultValue={user.address.geolocation.lat}
								InputProps={{
									readOnly: true,
								}}
								variant="standard"
							/>
						}
					</Grid>
					<Grid item xs={6} md={6}>
						{
							status === 'pending' &&
							<Fragment>
								<Skeleton variant="text" width="25%" height={10} />
								<Skeleton variant="text" height={10} />
							</Fragment>
						}
						{
							(status === 'finished' && user !== null) &&
							<TextField
								fullWidth
								label="Longitude"
								defaultValue={user.address.geolocation.long}
								InputProps={{
									readOnly: true,
								}}
								variant="standard"
							/>
						}
					</Grid>
				</Grid>
			</CardContent>
			{
				status === 'pending' &&
				<Skeleton variant="text" height={60} sx={{ mx: 1, my: 1, borderRadius: 6, display: { xs: 'flex', md: 'none' } }} />
			}
			{
				(status === 'finished' && user !== null) &&
				<CardActions sx={{ my: 1, mx: 1, display: { xs: 'flex', md: 'none' } }}>
					<Button
						onClick={LogoutIndex}
						fullWidth
						variant="outlined"
						color="error"
						sx={{
							borderRadius: 10,
						}}
					>
						Logout
					</Button>
				</CardActions>
			}
		</Card>
	)
}