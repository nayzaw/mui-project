import * as React from 'react';
import PropTypes from 'prop-types';
import useScrollTrigger from '@mui/material/useScrollTrigger';
import Zoom from '@mui/material/Zoom';
import Box from '@mui/material/Box';


export default function ScrollTop(props) {
	const { children } = props;

	const trigger = useScrollTrigger({
		disableHysteresis: true,
		threshold: 100,
	});

	const handleClick = (event) => {
		const anchor = (event.target.ownerDocument || document).querySelector(
			'#back-to-top-anchor',
		);

		if (anchor) {
			anchor.scrollIntoView({
				behavior: 'smooth',
				block: 'center',
			});
		}
	};

	return (
		<React.Fragment>
			<Zoom in={trigger}>
				<Box
					onClick={handleClick}
					role="presentation"
					sx={{ position: 'fixed', bottom: 16, right: 16, zIndex: 1051 }}
					className='backToTop'
				>
					{children}
				</Box>
			</Zoom>
		</React.Fragment>
	);
}

ScrollTop.propTypes = {
  	children: PropTypes.element.isRequired,
};