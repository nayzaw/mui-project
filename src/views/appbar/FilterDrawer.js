import * as React from 'react';
import { useDispatch, useSelector } from "react-redux";
import {
	List, ListItem, Drawer, Box, Typography,
	ListItemButton, ListItemIcon, Checkbox, ListItemText
} from '@mui/material';
import { changeCategory } from '../../features/categorySlice';


export default function FilterDrawer(props) {

	const categoryData = useSelector(state => state.category.categoryData)
	const checked = useSelector(state => state.category.checkedData)
	const dispatch = useDispatch();

	const list = () => (
		<Box
			sx={{ width: 'auto' }}
			role="presentation"
			onKeyDown={props.close}
		>
			<List>
				<ListItem>
					<Typography variant="h5" color="text.secondary" gutterBottom>
						Categories
					</Typography>
				</ListItem>
				{
					categoryData.map((text, index) => {
						
						const labelId = `checkbox-list-label-${index}`;

						return(
							<ListItem sx={{ p: 0 }} key={index}>
								<ListItemButton role={undefined} onClick={() => dispatch(changeCategory(index))} dense>
									<ListItemIcon sx={{ minWidth: 0 }}>
										<Checkbox
											edge="start"
											checked={checked.indexOf(index) !== -1}
											tabIndex={-1}
											disableRipple
											inputProps={{ 'aria-labelledby': labelId }}
											size="small"
										/>
									</ListItemIcon>
									<ListItemText
										primary={text}
									/>
								</ListItemButton>
							</ListItem>
						)
					})
				}
			</List>
		</Box>
	);

	return (
		<Drawer
			anchor='bottom'
			open={props.open}
			onClose={props.close}
		>
			{list()}
		</Drawer>
	);
}
