import * as React from 'react';
import AppBar from '@mui/material/AppBar';
import Toolbar from '@mui/material/Toolbar';
import CssBaseline from '@mui/material/CssBaseline';
import Box from '@mui/material/Box';
import Fab from '@mui/material/Fab';
import KeyboardArrowUpIcon from '@mui/icons-material/KeyboardArrowUp';
import Cart from '@mui/icons-material/ShoppingCart';
import IconButton from '@mui/material/IconButton';
import Badge from '@mui/material/Badge';
import { useSelector } from 'react-redux'
import { Link, NavLink, useLocation } from 'react-router-dom';
import { styles } from './styles';
import LoginDialogDemo from '../auth/login/LoginDialogDemo';
import { LoginProfile } from '../auth/login/LoginProfile';
import FilterListIcon from '@mui/icons-material/FilterList';
import FilterDrawer from './FilterDrawer';
import ContactIndex from '../contact/ContactIndex';
// import { useSnackbar } from 'notistack';
import ScrollTop from './ScrollTop';
import { validateEmail } from '../../validation/validation';
import { Spinner } from '../../my-common/Spinner';



export default function NavBar(props) {
	// const { enqueueSnackbar } = useSnackbar();
	const cart = useSelector(state => state.cart.cartData)
	const token = useSelector(state => state.user.token)
	const [open, setOpen] = React.useState(false)
	const location = useLocation();
	const [nameError, setNameError] = React.useState(false);
	const [msgError, setMsgError] = React.useState(false);
	const [emailError, setEmailError] = React.useState(false);
	const [emailMsg, setEmailMsg] = React.useState(false);
	const [contactOpen, setContactOpen] = React.useState(false);
	const [loading, setLoading] = React.useState(false);

	const toggleDrawer = (event) => {
		if (event.type === 'keydown' && (event.key === 'Tab' || event.key === 'Shift')) {
			return;
		}
		setOpen(!open);
	};

	const handleContact = (event) => {
		setContactOpen(!contactOpen);
	}

	const onSubmit = (e) => {
		let temp = false;
		const name = document.getElementById('name').value;
		const message = document.getElementById('message').value;
		const email = document.getElementById('email').value;

		name ? setNameError(false) : setNameError(true);
		message ? setMsgError(false) : setMsgError(true);

		if(!email){
			setEmailError(true);
			setEmailMsg('Please fill email')
		}
		else if(!validateEmail(email)){
			setEmailError(true);
			setEmailMsg('Email format is invalid')
		}
		else{
			setEmailError(false);
			setEmailMsg('');
			temp = true;
		}
		
		if(name && message && temp){
			setContactOpen(!contactOpen);
			setLoading(true);
			document.getElementById('contactForm').submit();
		}
	}

	React.useEffect(() => {
		if(contactOpen){
			setNameError(false)
			setMsgError(false)
			setEmailError(false)
		}
	}, [contactOpen])

	return (
		<React.Fragment>
			
			{ loading && <Spinner /> }

			<ContactIndex
				open={contactOpen}
				close={() => setContactOpen(!contactOpen)}
				nameError={nameError}
				msgError={msgError}
				emailError={emailError}
				emailMsg={emailMsg}
				onSubmit={onSubmit}
			/>
			
			<FilterDrawer
				open={open}
				close={() => setOpen(!open)}
			/>

			<CssBaseline />

			<AppBar color="inherit" className='navShadow'>
				<Toolbar>
					<Box>
						<NavLink to='/'>
							<Box sx={styles.imageContainer}>
								<Box
									className="selector"
									component="img"
									sx={styles.image}
									alt="Empty Cart"
									src="/images/logo1.png"
								/>
							</Box>
						</NavLink>
					</Box>

					<Box sx={styles.navMenu} >
						<Box className='navMenu'>
							<NavLink
								to={'/'}
								style={
									({ isActive }) => isActive ? styles.activeStyle : undefined
								}
							>
								Home
							</NavLink>

							<NavLink
								to={window.location}
								onClick={handleContact}
							>
								Contact
							</NavLink>
						</Box>
					</Box>
					
					<Box sx={{ ml: 'auto', display: 'flex' }}>
						<Link to={'/cart'}>
							<IconButton size="large">
								<Badge
									badgeContent={cart.length}
									color="error"
								>
									<Cart />
								</Badge>
							</IconButton>
						</Link>

						{
							location.pathname === '/' &&
							<IconButton size="large" sx={{ display: { xs: 'flex', sm: 'none', md: 'none' } }} onClick={() => toggleDrawer(true)}>
								<FilterListIcon />
							</IconButton>
						}
					</Box>
					
					{
						token
						?
						<LoginProfile />
						:
						<LoginDialogDemo />
					}

				</Toolbar>
			</AppBar>

			<Toolbar id="back-to-top-anchor" />
			
			<ScrollTop {...props}>
				<Fab color="primary" size="medium" aria-label="scroll back to top">
					<KeyboardArrowUpIcon />
				</Fab>
			</ScrollTop>

		</React.Fragment>
	);
}