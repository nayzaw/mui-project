export const styles = {
	image: {
		height: 50
	},
	imageContainer: {
		flexGrow: 1,
		display: 'flex',
		alignItems: 'center',
	},
	navMenu: {
		ml: 2.5,
		flexGrow: 1,
		// justifyContent: 'center',
		display: { xs: 'none', md: 'flex' },
	},
	activeStyle: {
		color: '#1976D2',
		// borderBottom: '3px solid #1976D2',
		// bottom: -1.6,
		// zIndex: -2
	},
}