import * as React from 'react';
import Dialog from '@mui/material/Dialog';
import DialogContent from '@mui/material/DialogContent';
import DialogContentText from '@mui/material/DialogContentText';
import DialogTitle from '@mui/material/DialogTitle';
import { Button, Grid, TextField, useMediaQuery } from '@mui/material';
import SendIcon from '@mui/icons-material/Send';


const ContactIndex = (props) => {

	const matches = useMediaQuery('(min-width:600px)');

	return (
		<Dialog open={props.open} maxWidth='xs'>

			<DialogTitle sx={{ fontWeight: 600 }}>Contact Me</DialogTitle>

			<DialogContent>
				<DialogContentText>
					To contact to website developer, please enter your information below. We
					will get back you soon.
				</DialogContentText>

				<form
					id='contactForm'
					// target='_blank'
					action={process.env.REACT_APP_FORM_ACTION_URL}
					method='post'
				>
					<Grid container mt={2} mb={2} spacing={2}>
						<Grid item xs={12} md={12}>
							<TextField
								helperText={props.nameError ? 'Please fill Name' : ''}
								error={props.nameError}
								required
								label='Name'
								variant='outlined'
								placeholder='Enter your name'
								fullWidth
								size='small'
								name='name'
								id='name'
							/>
						</Grid>
						<Grid item xs={12} md={12}>
							<TextField
								helperText={props.emailError ? props.emailMsg : ''}
								error={props.emailError}
								required
								label='Email'
								variant='outlined'
								placeholder='Enter your email'
								fullWidth
								size='small'
								name='email'
								id='email'
							/>
						</Grid>
						<Grid item xs={12} md={12}>
							<TextField
								helperText={props.msgError ? 'Please fill message' : ''}
								error={props.msgError}
								required
								label='Message'
								multiline
								rows={4}
								variant='outlined'
								placeholder='Enter you message'
								fullWidth
								size='small'
								name='message'
								id='message'
							/>
						</Grid>

						<Grid item xs={6} md={6}>
							<Button variant='outlined' size={matches ? 'medium' : 'large'} fullWidth onClick={props.close} sx={{ borderRadius: 10, mt: 2, mb: 1 }}>
								Cancel
							</Button>
						</Grid>
						<Grid item xs={6} md={6}>
							<Button variant='contained' size={matches ? 'medium' : 'large'} fullWidth onClick={props.onSubmit} endIcon={<SendIcon />} sx={{ borderRadius: 10, mt: 2, mb: 1 }}>
								Send
							</Button>
						</Grid>
					</Grid>
				</form>
			</DialogContent>
		</Dialog>
	);
}

export default ContactIndex
