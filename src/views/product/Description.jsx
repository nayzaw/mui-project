import React from 'react';
import { Tabs, Tab, Box, Typography, Skeleton } from '@mui/material';
import {TabPanel, TabContext} from '@mui/lab';
import { useSelector } from 'react-redux';

export const Description = () => {
	const [value, setValue] = React.useState('description');
	const singleProduct = useSelector(state => state.products.singleProduct)
	const status = useSelector(state => state.products.status)


	const handleChange = (event, newValue) => {
		setValue(newValue);
	};

	return (
		<Box sx={{ width: '100%' }} mt={3}>
			<TabContext value={value}>
				{
					status === 'pending' &&
					<React.Fragment>
						<Skeleton variant="text" height={45} width="14%" />
					</React.Fragment>
				}
				{
					(status === 'finished' && singleProduct !== null && singleProduct !== undefined) &&
					<Tabs
						value={value}
						onChange={handleChange}
						textColor="primary"
						indicatorColor="primary"
						aria-label="tabs"
					>
						<Tab value="description" label="Description" />
						<Tab value="review" label="Reviews" />
					</Tabs>
				}
				<TabPanel value="description" sx={{ pl: 0 }}>
					{
						status === 'pending' &&
						<React.Fragment>
							<Skeleton variant="text" height={10} sx={{ mb: 2 }} />
							<Skeleton variant="text" height={10} />
						</React.Fragment>
					}
					{
						(status === 'finished' && singleProduct !== null && singleProduct !== undefined) &&
						<Typography variant="body1" gutterBottom>
							{singleProduct.description}
						</Typography>
					}
				</TabPanel>
				<TabPanel value="review" sx={{ pl: 0 }}>
					<Typography variant="body1" gutterBottom>
						No reviews yet!
					</Typography>
				</TabPanel>
			</TabContext>
		</Box>
	);
}
