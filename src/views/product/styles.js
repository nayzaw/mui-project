export const styles = {
	center: {
		display: 'flex',
		justifyContent: 'center'
	},
	size: {
		width: '100%',
		maxHeight: 400,
		objectFit: 'contain'
	},
	title: {
		flexGrow: 1,
		fontWeight: 'bold'
	},
	vcenter: {
		display: 'flex',
		alignItems: 'center'
	},
}