import { useEffect, Fragment } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useNavigate, useParams } from 'react-router-dom';
import { fetchSingleProduct } from '../../thunks/product/fetchSingleProduct';
// import { Spinner } from '../../my-common/Spinner';
import { DetailData } from './DetailData';
import { Description } from './Description';
import { Error } from '../../my-common/Error';
import { Box, Button } from '@mui/material';


const ProductDetail = () => {

	const { productID } = useParams();
	const dispatch = useDispatch();
	const status = useSelector(state => state.products.status)
	const error = useSelector(state => state.products.error)
	const navigate = useNavigate()

	useEffect(() => {
		if(productID){
			dispatch(fetchSingleProduct(productID))
		}else {
			navigate('/');
		}
	}, [])


	let content;
	if (status === 'finished' || status === 'pending'){
		content = <Fragment>
			<DetailData />
			<Description />
		</Fragment>
	}
	else if (status === 'failed'){
		content = <Fragment>
			<Error error={error} />
			<Box sx={{ display: 'flex', minHeight: '80vh', alignItems: 'center', justifyContent: 'center' }}>
				<Button variant='outlined' color='error' onClick={() => window.location.reload()}>Please Try Again</Button>
			</Box>
		</Fragment>
	}

	return(
		<Fragment>
			{content}
		</Fragment>
	)
}

export default ProductDetail