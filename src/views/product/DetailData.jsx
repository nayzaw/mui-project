import {
	Grid, Typography, Rating, Box, Button, Chip, Skeleton, useMediaQuery
} from '@mui/material';
import { styles } from './styles';
import { useDispatch, useSelector } from 'react-redux';
import React, { Fragment } from 'react';
import { addToCart } from '../../features/cartSlice';
import { useSnackbar } from 'notistack';
import { Link } from 'react-router-dom';

export const DetailData = () => {
	const singleProduct = useSelector(state => state.products.singleProduct)
	const status = useSelector(state => state.products.status)
	const cart = useSelector(state => state.cart.cartData)
	const dispatch = useDispatch();
	const { enqueueSnackbar } = useSnackbar();
	const matches = useMediaQuery('(min-width:600px)');

	const ADD_TO_CART = () => {
		enqueueSnackbar('Successfully added!', { variant: 'success' });
		dispatch(addToCart({ current: singleProduct, cart }))
	}

	return(
		<Grid container spacing={2} className='productDetail'>
			<Grid item xs={12} md={6}>
				<Box sx={styles.center}>
					{
						status === 'pending' &&
						<Skeleton variant="rectangular" height={350} width="100%" />
					}
					{
						(status === 'finished' && singleProduct !== null && singleProduct !== undefined) &&
						<Box
							className="selector img"
							component="img"
							sx={styles.size}
							alt={singleProduct.title}
							src={singleProduct.image}
						/>
					}
				</Box>
			</Grid>

			<Grid item xs={12} md={6}>
				{
					status === 'pending' &&
					<Skeleton variant="text" height={40} />
				}
				{
					(status === 'finished' && singleProduct !== null && singleProduct !== undefined) &&
					<Typography variant="h5" component="h5" sx={styles.title}>
						{singleProduct.title}
					</Typography>
				}
				
				<Grid style={styles.vcenter} mt={2}>
					{
						status === 'pending' &&
						<Skeleton variant="text" height={10} width="14%" />
					}
					{
						(status === 'finished' && singleProduct !== null && singleProduct !== undefined) &&
						<Chip label={singleProduct.category} size="small" />
					}
				</Grid>
			
				<Grid style={styles.vcenter} mt={2}>
					{
						status === 'pending' &&
						<Skeleton variant="text" height={10} width="16%" />
					}
					{
						(status === 'finished' && singleProduct !== null && singleProduct !== undefined) &&
						<Fragment>
							<Rating name="read-only" value={singleProduct.rating.rate} readOnly size='small' />
							({singleProduct.rating.count})
						</Fragment>
					}
				</Grid>
				
				<Grid style={styles.vcenter} mt={2}>
					{
						status === 'pending' &&
						<Skeleton variant="text" height={30} width="12%" />
					}
					{
						(status === 'finished' && singleProduct !== null && singleProduct !== undefined) &&
						<Typography variant="h2" component="h2" className='detailPrice'>
							${singleProduct.price}
						</Typography>
					}
				</Grid>

				<Grid container style={styles.vcenter} mt={4} spacing={2}>
					<Grid item xs={12} sm={6} lg={5}>
						{
							status === 'pending' &&
							<Skeleton variant="text" height={60} sx={{ borderRadius: 6, }} />
						}
						{
							(status === 'finished' && singleProduct !== null && singleProduct !== undefined) &&
							<Button variant="contained" fullWidth onClick={ADD_TO_CART} sx={{ borderRadius: 10 }} size={matches ? 'medium' : 'large'} >
								Add to Cart
							</Button>
						}
					</Grid>
					<Grid item xs={12} sm={6} lg={5}>
						{
							status === 'pending' &&
							<Skeleton variant="text" height={60} sx={{ borderRadius: 6, }} />
						}
						{
							(status === 'finished' && singleProduct !== null && singleProduct !== undefined) &&
							<Link to={'/'}>
								<Button variant="outlined" fullWidth sx={{ borderRadius: 10 }} size={matches ? 'medium' : 'large'}>
									Continue Shopping
								</Button>
							</Link>
						}
					</Grid>
				</Grid>
			</Grid>
		</Grid>
	)
}