import * as React from 'react';
import {
	Grid, useMediaQuery, Container,
} from '@mui/material';
import { useSelector } from "react-redux";
import { OrderSummary } from './OrderSummary';
import { CartItem } from './CartItem';


export const CartList = () => {

	const cart = useSelector(state => state.cart.cartData);
	const matches = useMediaQuery('(min-width:600px)');

	return (<>
		{
			cart.length > 0 &&
			<Container maxWidth={matches ? 'md' : matches} disableGutters={!matches}>
				<Grid container spacing={2}>
					<Grid item xs={12} md={8}>
						<CartItem />
					</Grid>

					<Grid item xs={12} md={4} sx={{ zIndex: 1052 }}>
						<OrderSummary />
					</Grid>
				</Grid>
			</Container>
		}
	</>);
}
