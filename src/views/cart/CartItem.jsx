import React from 'react';
import {
	Grid, Box, Typography, Card, Collapse, IconButton
} from '@mui/material';
import { useDispatch, useSelector } from "react-redux";
import { styles } from './styles';
import ClearIcon from '@mui/icons-material/Clear';
import { removeFromCart, increment, decrement } from '../../features/cartSlice';
import RemoveCircleOutlineOutlinedIcon from '@mui/icons-material/RemoveCircleOutlineOutlined';
import AddCircleOutlineOutlinedIcon from '@mui/icons-material/AddCircleOutlineOutlined';
import { TransitionGroup } from 'react-transition-group';
import { Link } from 'react-router-dom';

export const CartItem = () => {

	const dispatch = useDispatch();
	const cart = useSelector(state => state.cart.cartData);

	const s = {
		root: { p: 1.6, display: 'flex', },
		img: { display: 'flex', alignItems: 'center' },
		title: { fontSize: '0.9rem', fontWeight: 'bold' },
		price: { fontSize: '0.9rem', fontWeight: 'bold' },
		dflex: { display: 'flex' },
		alignCenter: { alignItems: 'flex-start' },
		removeBtn: { display: 'flex', justifyContent: 'flex-end' },
		qtyAction: { display: 'flex', alignItems: 'center' }
	}


	const REMOVE_FROM_CART = (event, data) => {
		dispatch(removeFromCart({ current: data, cart }))
	}

	const handleIncrement = (i) => {
		dispatch(increment({ current: i, cart }))
	}

	const handleDecrement = (i) => {
		dispatch(decrement({ current: i, cart }))
	}

	return(
		<TransitionGroup>
			{
				cart.length > 0 &&
				cart.map((i, index) => {
					return (
						<Collapse key={i.title}>
							<Card className='cardShadow' sx={{ mb: 1 }}>
								<Grid container sx={s.root} className='cartList'>
									<Grid item md={2} xs={2} sx={s.img}>
										<Box
											className="selector img"
											component="img"
											sx={styles.size}
											alt={i.title}
											src={i.image}
										/>
									</Grid>

									<Grid item md={8} xs={8} sx={{ display: 'flex', flexWrap: 'wrap' }}>
										<Box pl={3}>
											<Link to={`/product-detail/${i.id}`}>
												<Typography variant="h6" gutterBottom component="div" sx={s.title}>
													{i.title}
												</Typography>
											</Link>
											<Typography variant="caption" display="block" gutterBottom>
												{i.category}
											</Typography>
										</Box>
										<Grid container sx={{ alignSelf: 'flex-end', pl: 3 }}>
											<Grid item xs={6} md={3} sx={s.qtyAction}>
												<Typography variant="h6" component="div" color='error' sx={s.price}>
													${i.price}
												</Typography>
											</Grid>
											<Grid item xs={6} md={3} sx={s.qtyAction}>
												<IconButton aria-label="decrement" size='small' onClick={() => handleDecrement(i)}>
													<RemoveCircleOutlineOutlinedIcon />
												</IconButton>
												<Typography variant="body2">
													{i.quantity}
												</Typography>
												<IconButton aria-label="increment" size='small' onClick={() => handleIncrement(i)}>
													<AddCircleOutlineOutlinedIcon />
												</IconButton>
											</Grid>
										</Grid>
									</Grid>

									<Grid item md={2} xs={2} sx={s.dflex}>
										<Grid container sx={s.alignCenter}>
											<Grid item md={12} xs={12} sx={s.removeBtn}>
												<IconButton aria-label="clear" size='small' onClick={(e) => REMOVE_FROM_CART(e, i)}>
													<ClearIcon />
												</IconButton>
											</Grid>
										</Grid>
									</Grid>
								</Grid>
							</Card>
						</Collapse>
					)
				})
			}
		</TransitionGroup>
	)
}