export const styles = {
	img: {
		width: 80,
		height: 80,
		objectFit: 'contain'
	},
	size: {
		width: '100%',
		maxHeight: 100,
		objectFit: 'contain'
	},
}