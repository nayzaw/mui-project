import React from 'react';
import { useSelector } from "react-redux";
import { EmptyCart } from './emptyCart';
import { CartList } from './CartList';


export default function Cart() {
	const cart = useSelector(state => state.cart.cartData);


	return (<>
		{
			cart.length > 0 &&
			<CartList />
		}
		
		{
			cart.length === 0 &&
			<EmptyCart />
		}
	</>);
}
