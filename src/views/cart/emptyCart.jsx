import { Box, Button, Typography } from "@mui/material"

export const EmptyCart = () => {

	const styles = {
		image: {
			// height: 200,
			maxHeight: { xs: 100, md: 130, lg: 200, xl: 250 },
		},
		heading: {
			fontFamily: 'unset',
			fontWeight: 'bold',
			fontSize: { xs: 15, lg: 20}
		}
	}

	return(<>
		<Box className="vCenter">
			<Box className="dFlexCenter">
				<Box
					className="selector"
					component="img"
					sx={styles.image}
					alt="Empty Cart"
					src="/images/empty.png"
				/>
			</Box>
			<Box className="dFlexCenter" mt={2} >
				<Typography component="h5" variant="h5" sx={styles.heading}>
					NO ITEMS IN CART
				</Typography>
			</Box>
			<Box className="dFlexCenter" mt={3} >
				<Button variant="outlined" href="/" color="error" size='large'>
					Continue Shopping
				</Button>
			</Box>
		</Box>
	</>	)
}