import { Card, CardContent, Divider, Typography, Box, Button, useMediaQuery } from '@mui/material';
import React from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { calculateTotal } from '../../features/cartSlice';
import { placeOrder } from '../../features/orderSlice';
import { fetchAllUser } from '../../thunks/user/fetchAllUser';
import LoginDialogIndex from '../auth/login/LoginDialogIndex';
import { useSnackbar } from 'notistack';
import { Spinner } from '../../my-common/Spinner';



export const OrderSummary = () => {
	const { enqueueSnackbar } = useSnackbar();
	const dispatch = useDispatch();
	const cart = useSelector(state => state.cart.cartData);
	const subTotal = useSelector(state => state.cart.subTotal);
	const total = useSelector(state => state.cart.total);
	const token = useSelector(state => state.user.token);
	const emails = useSelector(state => state.user.userEmail);
	const [open, setOpen] = React.useState(false);
	const [selectedValue, setSelectedValue] = React.useState(emails[0]);
	const [orderSpinner, setOrderSpinner] = React.useState(false);
	const matches = useMediaQuery('(min-width:600px)');

	const handleClickOpen = (event) => {
		if(token){
			setOrderSpinner(true);
			setTimeout(order, 2000);
		}else{
			dispatch(fetchAllUser());
			setOpen(true);
		}
	};

	const order = () => {
		dispatch(placeOrder({ cart, total }));
		enqueueSnackbar('Your order has been placed. Thank you for shopping at Linnz', { variant: 'success' });
		setOrderSpinner(false);
	}

	const handleClose = (value) => {
		setOpen(false);
		setSelectedValue(value);
	};

	React.useEffect(() => {
		dispatch(calculateTotal({ cart }));
	}, [cart])

	return(
		<React.Fragment>
			
			{ orderSpinner && <Spinner /> }
		
			<Card className='cardShadow stickyTop'>
				<LoginDialogIndex
					selectedValue={selectedValue}
					open={open}
					onClose={handleClose}
				/>
				<CardContent>
					<Typography variant="h5" component="div" gutterBottom sx={{ fontWeight: 500 }}>
						Order Summary
					</Typography>

					<Divider />

					<Box mt={2} sx={{ display: 'flex' }}>
						<Typography>Subtotal</Typography>
						<Typography sx={{ ml: 'auto', }}>${subTotal}</Typography>
					</Box>

					<Box mt={2} mb={2} sx={{ display: 'flex' }}>
						<Typography>Commercial Tax</Typography>
						<Typography sx={{ ml: 'auto', }}>5%</Typography>
					</Box>

					<Divider />

					<Box mt={2} mb={2} sx={{ display: 'flex' }}>
						<Typography sx={{ fontWeight: 'bold' }}>Total</Typography>
						<Typography sx={{ ml: 'auto', fontWeight: 'bold' }}>${total}</Typography> {/* old color: '#d32f2f' */}
						
					</Box>

					<Divider />

					<Button variant='contained' sx={{ borderRadius: 10, mt: 3 }} fullWidth onClick={handleClickOpen} size={matches ? 'medium' : 'large'}>
						{
							token ? 'Place Order' : 'Login to continue'
						}
					</Button>
				</CardContent>
			</Card>
		</React.Fragment>
	)
}