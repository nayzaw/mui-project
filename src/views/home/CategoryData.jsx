import React, { useState } from 'react';
import {
	Card, CardContent, Typography, List, ListItem, 
	ListItemButton, ListItemIcon, Checkbox, ListItemText, Skeleton
} from '@mui/material';
import { useDispatch, useSelector } from "react-redux";
import { changeCategory } from '../../features/categorySlice';

export const CategoryData = () => {

	const categoryData = useSelector(state => state.category.categoryData)
	const checked = useSelector(state => state.category.checkedData)
	const status = useSelector(state => state.products.status);
	const dispatch = useDispatch();
	const [pendingData] = useState([1,2,3,4,5,6,7]);

	return(
		<Card className='cardShadow stickyTop'>
			<CardContent>
				{
					status === 'pending' &&
					<>
						<Typography component="div" key="h3" variant="h3">
							<Skeleton />
						</Typography>
						{
							pendingData.map((i, index) => {
								return (
									<Skeleton variant="text" key={index} width="55%" />
								)
							})
						}
					</>
				}

				{
					status === 'finished' && <>
						
						<List sx={{ p: 0 }}>
							<ListItem>
								<Typography variant="h5" color="text.secondary" gutterBottom>
									Categories
								</Typography>
							</ListItem>
							
							{
								categoryData.map((text, index) => {

									const labelId = `checkbox-list-label-${index}`;

									return (
										<ListItem sx={{ p: 0 }} key={index}>
											<ListItemButton role={undefined} onClick={() => dispatch(changeCategory(index))} dense>
												<ListItemIcon sx={{ minWidth: 0 }}>
													<Checkbox
														edge="start"
														checked={checked.indexOf(index) !== -1}
														tabIndex={-1}
														disableRipple
														inputProps={{ 'aria-labelledby': labelId }}
														size="small"
													/>
												</ListItemIcon>
												<ListItemText
													primary={text}
												/>
											</ListItemButton>
										</ListItem>
									)
								})
							}
						</List>
					</>
				}
				
			</CardContent>
		</Card>
	)
}