import {
	Rating, IconButton, Grid, Card, CardMedia, CardContent, Typography, Box
} from '@mui/material';

import React from 'react';
import { useDispatch, useSelector } from 'react-redux'
import { addToCart } from '../../features/cartSlice';
import { useSnackbar } from 'notistack';
import { Link } from 'react-router-dom';
import ShoppingCartOutlinedIcon from '@mui/icons-material/ShoppingCartOutlined';
import { LightTooltip } from '../../my-common/LightTooltip';


export const ProductList = ({data, status}) => {

	const { enqueueSnackbar } = useSnackbar();

	const dispatch = useDispatch();
	const cart = useSelector(state => state.cart.cartData)

	
	const ADD_TO_CART = () => {
		enqueueSnackbar('Successfully added!', { variant: 'success' });
		dispatch(addToCart({ current: data, cart }))
		// dispatch(addNewCart(data))
	}


	const buttons =
		<LightTooltip title="Add to cart" disableInteractive>
			<IconButton aria-label='add shopping cart' color='primary' onClick={() => ADD_TO_CART(data)}>
				<ShoppingCartOutlinedIcon  />
			</IconButton>
		</LightTooltip>

	return(		
		<Grid item xs={6} sm={4} md={3} lg={2.4} xl={2}>
			<Card className='cardShadow'>
					<Link to={`/product-detail/${data.id}`} >
						<Box className='overlay' sx={{ m: 1 }}>
							<CardMedia
								component="img"
								alt={data.image}
								height="175"
								image={data.image}
								style={{ padding: 10, objectFit: 'contain' }}
							/>
							<Box className="textMid">
								<Typography variant="caption" display="block">
									View Detail
								</Typography>
							</Box>
						</Box>
					</Link>
					<CardContent className='pt0 pb5px'>
						<Typography gutterBottom variant="subtitle1" component="div" className="ellipse productTitle">
							{data.title}
						</Typography>
						<Rating name="read-only" size="small" value={data.rating.rate} readOnly />
						<Grid container>
							<Grid item xs={6} className='left'>
								<Typography variant="body2" color="error" className="ellipse bold">
									${data.price}
								</Typography>
							</Grid>
							<Grid item xs={6} className='right'>
								{buttons}
							</Grid>
						</Grid>
					</CardContent>
				
			</Card>
		</Grid>
	)
}