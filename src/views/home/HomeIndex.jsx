import React, { useEffect } from 'react';
import {
	Grid,
} from '@mui/material';
import { ProductList } from './ProductList';
import { useSelector, useDispatch } from "react-redux";
import { filterProducts } from '../../features/productSlice';
import { fetchProducts } from '../../thunks/product/fetchProducts';
import { CategoryData } from './CategoryData';
import ProductSkeleton from '../../my-common/ProductSkeleton';
import { fetchCategory } from '../../thunks/category/fetchCategory';
import { Error } from '../../my-common/Error';



export default function Home() {
	
	const dispatch = useDispatch();
	const products = useSelector(state => state.products.products)
	const status = useSelector(state => state.products.status);
	const error = useSelector(state => state.products.error);
	const checkedData = useSelector(state => state.category.checkedData);
	const categoryData = useSelector(state => state.category.categoryData);
	const filterPds = useSelector(state => state.products.filterProductsData);


	useEffect(() => {
		dispatch(fetchProducts());
		dispatch(fetchCategory());
	}, [])


	useEffect(() => {
		if(checkedData.length > 0){
			let result, arr = [];

			checkedData.forEach( i => {
				result = products.filter(p => p.category === categoryData[i]);
				arr.push(...result);
			});

			dispatch(filterProducts(arr));
		}else{
			dispatch(filterProducts([]));
		}
	}, [checkedData])


	let content;

	if (status === 'pending') {
		content = <ProductSkeleton />
	}
	else if (status === 'finished') {
		if(filterPds.length > 0){
			content = filterPds.map((i, index) => <ProductList key={index} url={i.image} id={i.id} data={i} />)
		}else{
			content = products.map((i, index) => <ProductList key={index} url={i.image} id={i.id} data={i} />)
		}
	}
	else if (status === 'failed') {
		content = <Error error={error} />
	}


	return (<>

		<Grid container spacing={2} rowSpacing={3}>
			<Grid item xs={12} sm={4} md={3} sx={{ display: { xs: 'none', sm: 'block', md: 'block' }}}>
				{
					status !== 'failed' &&
					<CategoryData />
				}
			</Grid>

			<Grid item xs={12} sm={8} md={9} >
				<Grid container spacing={1} rowSpacing={1}>
					{content}
				</Grid>
			</Grid>
		</Grid>

		{/* <Stack spacing={2} sx={style}>
			<Pagination count={10} variant="outlined" color="primary" />
		</Stack> */}
	</>);
}


