export const LogoutIndex = () => {
	localStorage.removeItem('CURRENT_USER');
	localStorage.removeItem('TOKEN');
	window.location.reload();
}