import * as React from 'react';
import Typography from '@mui/material/Typography';
import { useDispatch, useSelector } from 'react-redux';
import { fetchAllUser } from '../../../thunks/user/fetchAllUser';
import LoginDialogIndex from './LoginDialogIndex';


const LoginDialogDemo = () => {
	const emails = useSelector(state => state.user.userEmail);
	const [open, setOpen] = React.useState(false);
	const [selectedValue, setSelectedValue] = React.useState(emails[0]);
	const dispatch = useDispatch();

	const handleClickOpen = () => {
		dispatch(fetchAllUser());
		setOpen(true);
	};

	const handleClose = (value) => {
		setOpen(false);
		setSelectedValue(value);
	};

	return (
		<React.Fragment>
			<Typography variant="button" display="block" sx={{ pl: 1, cursor: 'pointer' }} onClick={handleClickOpen}>Login</Typography>
			<LoginDialogIndex
				selectedValue={selectedValue}
				open={open}
				onClose={handleClose}
			/>
		</React.Fragment>
	);
}

export default LoginDialogDemo
