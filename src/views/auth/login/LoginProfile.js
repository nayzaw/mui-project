import React from 'react';
import { Box } from "@mui/system"
import MenuItem from '@mui/material/MenuItem';
import Menu from '@mui/material/Menu';
import IconButton from '@mui/material/IconButton';
import { AccountCircle } from '@mui/icons-material';
import { LogoutIndex } from '../logout/LogoutIndex';
import { useNavigate } from 'react-router-dom';
import LogoutIcon from '@mui/icons-material/Logout';
import PersonIcon from '@mui/icons-material/Person';
import ListItemText from '@mui/material/ListItemText';
import ListItemIcon from '@mui/material/ListItemIcon';
import InventoryIcon from '@mui/icons-material/Inventory';

export const LoginProfile = () => {

	const [anchorEl, setAnchorEl] = React.useState(null);
	const navigate = useNavigate();
	const user = JSON.parse(localStorage.getItem('CURRENT_USER')) || null

	const handleMenu = (event) => {
		setAnchorEl(event.currentTarget);
	};

	const handleClose = () => {
		setAnchorEl(null);
	};

	const handleProfile = () => {
		handleClose();
		if(user){
			navigate('/user')
		}else{
			LogoutIndex();
		}
	}

	const handleOrder = () => {
		handleClose();
		if(user){
			navigate('/order')
		}else{
			LogoutIndex();
		}
	}


	return(
		<Box sx={{ display: { xs: 'none', sm: 'flex', md: 'flex' } }}>
			<IconButton
				size="large"
				aria-label="account of current user"
				aria-controls="menu-appbar"
				aria-haspopup="true"
				onClick={handleMenu}
			>
				<AccountCircle />
			</IconButton>
			<Menu
				id="menu-appbar"
				anchorEl={anchorEl}
				anchorOrigin={{
					vertical: 'bottom',
					horizontal: 'left',
				}}
				keepMounted
				transformOrigin={{
					vertical: 'top',
					horizontal: 'left',
				}}
				open={Boolean(anchorEl)}
				onClose={handleClose}
			>
				<MenuItem onClick={handleProfile}>
					<ListItemIcon>
						<PersonIcon sx={{ fontSize: 'large' }} />
					</ListItemIcon>
					<ListItemText>My Profile</ListItemText>
				</MenuItem>
				<MenuItem onClick={handleOrder}>
					<ListItemIcon>
						<InventoryIcon sx={{ fontSize: 'large' }} />
					</ListItemIcon>
					<ListItemText>My Orders</ListItemText>
				</MenuItem>
				<MenuItem onClick={LogoutIndex}>
					<ListItemIcon>
						<LogoutIcon sx={{ fontSize: 'large' }} />
					</ListItemIcon>
					<ListItemText>Logout</ListItemText>
				</MenuItem>
			</Menu>
		</Box>
	)
}