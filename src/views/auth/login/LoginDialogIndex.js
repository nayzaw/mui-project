import * as React from 'react';
import Avatar from '@mui/material/Avatar';
import List from '@mui/material/List';
import ListItem from '@mui/material/ListItem';
import ListItemAvatar from '@mui/material/ListItemAvatar';
import ListItemText from '@mui/material/ListItemText';
import DialogTitle from '@mui/material/DialogTitle';
import Dialog from '@mui/material/Dialog';
import PersonIcon from '@mui/icons-material/Person';
import AddIcon from '@mui/icons-material/Add';
import { blue } from '@mui/material/colors';
import { useDispatch, useSelector } from 'react-redux';
import { Spinner } from '../../../my-common/Spinner';
import { loginUser } from '../../../thunks/user/loginUser';
import { Error } from '../../../my-common/Error';



const LoginDialogIndex = (props) => {

	const { onClose, selectedValue, open } = props;
	const emails = useSelector(state => state.user.userEmail);
	const status = useSelector(state => state.user.status);
	const error = useSelector(state => state.user.error);
	const users = useSelector(state => state.user.userData);
	const dispatch = useDispatch();

	const handleClose = () => {
		onClose(selectedValue);
	};

	const handleListItemClick = (value) => {
		if (value === 'addAccount'){
			alert('Add account is not available');
		}else{
			let result = users.filter(i => i.email === value);
			const { username, password } = result[0];
			localStorage.setItem('CURRENT_USER', JSON.stringify(result[0]));
			dispatch(loginUser({ username, password }))
			onClose(value);
		}
	};

	const dialog = <Dialog onClose={handleClose} open={open}>
		<DialogTitle>Choose account to login</DialogTitle>
		<List sx={{ pt: 0 }}>
			{emails.map((email, index) => (
				<ListItem button onClick={() => handleListItemClick(email)} key={index}>
					<ListItemAvatar>
						<Avatar sx={{ bgcolor: blue[100], color: blue[600] }}>
							<PersonIcon />
						</Avatar>
					</ListItemAvatar>
					<ListItemText primary={email} />
				</ListItem>
			))}

			<ListItem button onClick={() => handleListItemClick('addAccount')}>
				<ListItemAvatar>
					<Avatar>
						<AddIcon />
					</Avatar>
				</ListItemAvatar>
				<ListItemText primary="Add account" />
			</ListItem>
		</List>
	</Dialog>

	let content;
	if(status === 'pending'){
		content = <Spinner />
	}
	else if(status === 'finished'){
		content = dialog
	}
	else if(status === 'failed'){
		content = <Error error={error} />
	}

	return (
		<React.Fragment>
			{content}
		</React.Fragment>
	);
}

export default LoginDialogIndex