import * as React from 'react';
import { Box, Button, Typography, useMediaQuery } from "@mui/material"
import ContactIndex from "../views/contact/ContactIndex";


const SubmitErrorIndex = () => {

	const matches = useMediaQuery('(min-width:600px)');
	const [nameError, setNameError] = React.useState(false);
	const [msgError, setMsgError] = React.useState(false);
	const [contactOpen, setContactOpen] = React.useState(false)

	const styles = {
		image: {
			height: 100,
		},
	}

	const handleContact = (event) => {
		setContactOpen(!contactOpen);
	}

	const onSubmit = (e) => {
		const name = document.getElementById('name').value;
		const message = document.getElementById('message').value;

		name ? setNameError(false) : setNameError(true);
		message ? setMsgError(false) : setMsgError(true);

		if (name && message) {
			document.getElementById('contactForm').submit();
		}
	}

	React.useEffect(() => {
		if (contactOpen) {
			setNameError(false)
			setMsgError(false)
		}
	}, [contactOpen])

	return (<>

		<ContactIndex
			open={contactOpen}
			close={() => setContactOpen(!contactOpen)}
			nameError={nameError}
			msgError={msgError}
			onSubmit={onSubmit}
		/>


		<Box className="vCenter">
			<Box sx={{ textAlign: 'center' }}>
				<Box
					className="selector"
					component="img"
					sx={styles.image}
					alt="Error Icon"
					src="/images/error.png"
				/>
			</Box>
			<Box sx={{ textAlign: 'center' }} mt={2} >
				<Typography component="div" variant={matches ? 'h2' : 'h4'}>
					Sorry!
				</Typography>
			</Box>

			<Box sx={{ textAlign: 'center' }} mt={2} >
				<Typography component="div" variant={matches ? 'h5' : 'h6'}>
					Unfortunately, your submission cannot be done.
				</Typography>
			</Box>

			<Box sx={{ textAlign: 'center' }} mt={3} >
				<Button variant="outlined" onClick={handleContact} color="error" size='large'>
					Try again
				</Button>
			</Box>
		</Box>
	</>)
}

export default SubmitErrorIndex