import { Box, Button, Typography, useMediaQuery } from "@mui/material"

const EmptyOrderIndex = () => {

	const matches = useMediaQuery('(min-width:600px)');

	const styles = {
		image: {
			height: 100,
		},
		heading: {
			fontFamily: 'unset',
			fontWeight: 'bold',
			fontSize: { xs: 15, lg: 20 }
		}
	}

	return (
		<Box className="vCenter">
			<Box sx={{ textAlign: 'center' }}>
				<Box
					className="selector"
					component="img"
					sx={styles.image}
					alt="Order Icon"
					src="/images/order.png"
				/>
			</Box>

			<Box sx={{ textAlign: 'center' }} mt={2} >
				<Typography component="div" variant="h5" sx={styles.heading}>
					NO PURCHASED DATA
				</Typography>
			</Box>

			<Box sx={{ textAlign: 'center' }} mt={3} >
				<Button variant="outlined" href="/" color="primary" size='large'>
					Shop Now
				</Button>
			</Box>
		</Box>
	)
}

export default EmptyOrderIndex