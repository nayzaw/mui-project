import { Box, Button, Typography, useMediaQuery } from "@mui/material"

const SubmitSuccessIndex = () => {

	const matches = useMediaQuery('(min-width:600px)');

	const styles = {
		image: {
			height: 80,
		},
	}

	return (
		<Box className="vCenter">
			<Box sx={{ textAlign: 'center' }}>
				<Box
					className="selector"
					component="img"
					sx={styles.image}
					alt="Success Icon"
					src="/images/check.png"
				/>
			</Box>
			<Box sx={{ textAlign: 'center' }} mt={2} >
				<Typography component="div" variant={matches ? 'h2' : 'h4'}>
					Thank You!
				</Typography>
			</Box>

			<Box sx={{ textAlign: 'center' }} mt={2} >
				<Typography component="div" variant={matches ? 'h5' : 'h6'}>
					Your submission is received and we will contact you soon.
				</Typography>
			</Box>

			<Box sx={{ textAlign: 'center' }} mt={3} >
				<Button variant="outlined" href="/" color="primary" size='large'>
					Back to Home
				</Button>
			</Box>
		</Box>
	)
}

export default SubmitSuccessIndex