import { configureStore } from "@reduxjs/toolkit";
import cartReducer from "./features/cartSlice";
import categoryReducer from "./features/categorySlice";
import orderReducer from "./features/orderSlice";
import productReducer from "./features/productSlice";
import userReducer from "./features/userSlice";


export const store = configureStore({
	reducer: {
		products: productReducer,
		cart: cartReducer,
		category: categoryReducer,
		user: userReducer,
		order: orderReducer,
	}
})