import './App.css';
import React, { useEffect } from 'react'
import { Routes, Route } from "react-router-dom";
import NavBar from './views/appbar/NavBar';
import { Container } from '@mui/material';
import MyBottomNavigation from './my-common/MyBottomNavigation';
import { useLocation } from "react-router-dom";
import { useDispatch, useSelector } from 'react-redux';
import { clearSingleProduct } from './features/productSlice';
import { routes } from './routes';


function App() {

	const dispatch = useDispatch();
	const cart = useSelector(state => state.cart.cartData)
	const singleProduct = useSelector(state => state.products.singleProduct)
	const orderData = useSelector(state => state.order.orderData)
	const { pathname } = useLocation();

	useEffect(() => {
		window.scrollTo({top: 0})
	}, [pathname]);


	useEffect(() => {
		localStorage.setItem('CART', JSON.stringify(cart));
	}, [cart]);


	useEffect(() => {
		localStorage.setItem('ORDER_DATA', JSON.stringify(orderData));
	}, [orderData]);


	useEffect(() => {
		if (singleProduct)
		dispatch(clearSingleProduct());
	}, []);


	return (
		<div className="App">
			<header>
				<NavBar />
			</header>
			<main>
				<Container fixed maxWidth="xl" disableGutters>
					<Routes>
						{
							routes.map((i,index) => {
								return(
									<Route
										key={index}
										path={i.path}
										element={
											<React.Suspense>
												{i.element}
											</React.Suspense>
										}
									/>
								)
							})
						}
					</Routes>
				</Container>
			</main>
			<footer>
				<MyBottomNavigation />
			</footer>
		</div>
	);
}

export default App;
