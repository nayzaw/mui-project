// eslint-disable-next-line import/no-anonymous-default-export
export default {
	products: '/products',
	carts: '/carts',
	categories: 'products/categories',
	users: '/users',
	login: 'auth/login',
}