import axios from 'axios';

export const ApiRequest = async (value) => {
	let result, responseType, parameter, path = "";

	path = process.env.REACT_APP_API_URL;

	// Set the AUTH token for any request
	axios.interceptors.request.use(config => {
		const token = localStorage.getItem('TOKEN');
		config.headers.Authorization = token ? `Bearer ${token}` : '';
		config.headers.Accept = 'application/json';
		return config;
	});

	// set parameter based on api request method
	if(value.method === 'post' || value.method === 'patch' || value.method === 'put' || value.method === 'delete') {
		parameter = { baseURL: path, method: value.method, url: value.url, data: value.params, responseType };
	}else{
		parameter = { baseURL: path, method: value.method, url: value.url, params: value.params, responseType };
	}

	// calling api
	// await axios(parameter).then(async (response) => {
	// 	result = response;
	// }).catch(async (error) => {
	// 	result =  error.response;
	// });
	result = await axios(parameter);
	return result
}