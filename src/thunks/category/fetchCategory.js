import { createAsyncThunk } from "@reduxjs/toolkit";
import ApiPath from "../../api/ApiPath";
import { ApiRequest } from "../../api/ApiRequest";

export const fetchCategory = createAsyncThunk('category/fetchCategory', async () => {
	let obj = {
		url: ApiPath.categories
	}
	const response = await ApiRequest(obj);
	return response.data
})