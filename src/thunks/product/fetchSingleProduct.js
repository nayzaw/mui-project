import { createAsyncThunk } from "@reduxjs/toolkit";
import ApiPath from "../../api/ApiPath";
import { ApiRequest } from "../../api/ApiRequest";

export const fetchSingleProduct = createAsyncThunk('products/fetchSingleProduct', async (id) => {
	let obj = {
		url: `${ApiPath.products}/${id}`,
		method: 'get',
	}
	const response = await ApiRequest(obj);
	return response.data
})