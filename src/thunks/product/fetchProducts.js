import { createAsyncThunk } from "@reduxjs/toolkit";
import ApiPath from "../../api/ApiPath";
import { ApiRequest } from "../../api/ApiRequest";

export const fetchProducts = createAsyncThunk('products/fetchProducts', async () => {
	let obj = {
		url: ApiPath.products,
		method: 'get',
	}
	const response = await ApiRequest(obj);
	return response.data
})