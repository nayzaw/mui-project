import { createAsyncThunk } from "@reduxjs/toolkit";
import ApiPath from "../../api/ApiPath";
import { ApiRequest } from "../../api/ApiRequest";

export const fetchAllUser = createAsyncThunk('user/fetchAllUser', async () => {
	let obj = {
		url: ApiPath.users
	}
	const response = await ApiRequest(obj);
	return response.data
})