import { createAsyncThunk } from "@reduxjs/toolkit";
import ApiPath from "../../api/ApiPath";
import { ApiRequest } from "../../api/ApiRequest";

export const loginUser = createAsyncThunk('user/loginUser', async ({ username, password }) => {
	let obj = {
		url: ApiPath.login,
		method: 'post',
		params: {
			username, password
		}
	}
	const response = await ApiRequest(obj);
	return response.data
})