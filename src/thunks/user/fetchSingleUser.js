import { createAsyncThunk } from "@reduxjs/toolkit";
import ApiPath from "../../api/ApiPath";
import { ApiRequest } from "../../api/ApiRequest";

export const fetchSingleUser = createAsyncThunk('user/fetchSingleUser', async (id) => {
	let obj = {
		url: `${ApiPath.users}/${id}`
	}
	const response = await ApiRequest(obj);
	return response.data
})