import Backdrop from '@mui/material/Backdrop';
import CircularProgress from '@mui/material/CircularProgress';

export const Spinner = () => (
	<Backdrop
		sx={{ color: '', zIndex: (theme) => theme.zIndex.drawer + 1 }}
		open={true}
	>
		<CircularProgress color="primary" />
	</Backdrop>
)