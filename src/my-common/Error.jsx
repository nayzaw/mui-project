import React from 'react';
import { useSnackbar } from 'notistack';

export const Error = (props) => {

	const { enqueueSnackbar } = useSnackbar();

	React.useEffect(() => {
		enqueueSnackbar(props.error, { variant: 'error' });
	}, [])

	return (<>
	</>)
}