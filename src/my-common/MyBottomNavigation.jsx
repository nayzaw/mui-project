import * as React from 'react';
import { Paper } from '@mui/material';
import BottomNavigation from '@mui/material/BottomNavigation';
import BottomNavigationAction from '@mui/material/BottomNavigationAction';
import HomeOutlinedIcon from '@mui/icons-material/HomeOutlined';
import AccountCircleOutlinedIcon from '@mui/icons-material/AccountCircleOutlined';
import {  Link, useLocation } from 'react-router-dom';
import { useSelector } from 'react-redux';
import InventoryIcon from '@mui/icons-material/Inventory';
import EmailIcon from '@mui/icons-material/Email';
import ContactIndex from '../views/contact/ContactIndex';
import { useSnackbar } from 'notistack';
import { validateEmail } from '../validation/validation';
import { Spinner } from './Spinner';



export default function MyBottomNavigation() {
	let location = useLocation();
	const [pathName, setPathName] = React.useState(location.pathname);
	const token = useSelector(state => state.user.token);
	const [nameError, setNameError] = React.useState(false);
	const [msgError, setMsgError] = React.useState(false);
	const [emailError, setEmailError] = React.useState(false);
	const [emailMsg, setEmailMsg] = React.useState(false);
	const [contactOpen, setContactOpen] = React.useState(false);
	const [loading, setLoading] = React.useState(false);
	const { enqueueSnackbar } = useSnackbar();

	React.useEffect(() => {
		setPathName(location.pathname);
	}, [location])

	const styles = {
		position: 'fixed', 
		bottom: 0, 
		left: 0, 
		right: 0, 
		display: { xs: '12', sm: 'none', md: 'none', lg: 'none' },
		zIndex: 1053
	}


	const handleContact = (event) => {
		setContactOpen(!contactOpen);
	}

	const onSubmit = (e) => {
		let temp = false;
		const name = document.getElementById('name').value;
		const message = document.getElementById('message').value;
		const email = document.getElementById('email').value;

		name ? setNameError(false) : setNameError(true);
		message ? setMsgError(false) : setMsgError(true);

		if(!email){
			setEmailError(true);
			setEmailMsg('Please fill email')
		}
		else if(!validateEmail(email)){
			setEmailError(true);
			setEmailMsg('Email format is invalid')
		}
		else{
			setEmailError(false);
			setEmailMsg('');
			temp = true;
		}
		
		if(name && message && temp){
			setContactOpen(!contactOpen);
			setLoading(true);
			document.getElementById('contactForm').submit();
		}
	}

	React.useEffect(() => {
		if(contactOpen){
			setNameError(false)
			setMsgError(false)
			setEmailError(false)
		}
	}, [contactOpen])


	return (
		<Paper sx={styles} elevation={3}>
			{ loading && <Spinner /> }
			<ContactIndex
				open={contactOpen}
				close={() => setContactOpen(!contactOpen)}
				nameError={nameError}
				msgError={msgError}
				emailError={emailError}
				emailMsg={emailMsg}
				onSubmit={onSubmit}
			/>
			<BottomNavigation
				showLabels
				value={pathName}
				onChange={(event, newValue) => {
					setPathName(newValue);
				}}
			>
				<BottomNavigationAction label="Home" icon={<HomeOutlinedIcon />} component={Link}  value='/' to='/' />
				{/* <BottomNavigationAction label="Cart" icon={<Cart />} component={Link} value='/cart' to='/cart' /> */}

				{
					token &&
					<BottomNavigationAction label="My Orders" icon={<InventoryIcon />} component={Link} value='/order' to='/order' />
				}

				<BottomNavigationAction onClick={handleContact} label="Contact" icon={<EmailIcon />} component={Link} value={window.location} to={window.location} />
				
				{
					token &&
					<BottomNavigationAction label="Account" icon={<AccountCircleOutlinedIcon />} component={Link} value='/user' to='/user' />
				}
			</BottomNavigation>
		</Paper>
	);
}
