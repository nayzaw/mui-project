import * as React from 'react';
import {
	Skeleton, Grid, Card, CardContent
} from '@mui/material';

export default function ProductSkeleton() {

	const [pendingData] = React.useState([1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20]);

	return (<>
		{
			pendingData.map((i, index) => {
				return (
					<Grid item xs={6} sm={4} md={3} lg={2.4} xl={2} key={index}>
						<Card className='cardShadow'>
							<Skeleton variant="rectangular" height={175} />
							<CardContent className='pt0 pb5px'>
								<Skeleton variant="text" height={10} sx={{ mt: 0.7, mb: 0.7 }} />
								<Skeleton variant="text" height={10} width="90%" sx={{ mb: 0.7 }} />
								<Skeleton variant="text" height={10} width="50%" sx={{ mb: 0.7 }} />
							</CardContent>
						</Card>
					</Grid>
				)
			})
		}
	</>)
}
