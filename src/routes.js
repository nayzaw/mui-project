import * as React from 'react';


const Home = React.lazy(() => import('./views/home/HomeIndex'));
const Cart = React.lazy(() => import('./views/cart/CartIndex'));
const UserIndex = React.lazy(() => import('./views/user/UserIndex'));
const ProductDetail = React.lazy(() => import('./views/product/ProductDetailIndex'));
const OrderIndex = React.lazy(() => import('./views/order/OrderIndex'));
const SubmitSuccessIndex = React.lazy(() => import('./pages/SubmitSuccessIndex'));
const SubmitErrorIndex = React.lazy(() => import('./pages/SubmitErrorIndex'));
const EmptyOrderIndex = React.lazy(() => import('./pages/EmptyOrderIndex'));


export const routes = [
	{
		path: '/',
		element: <Home />
	},
	{
		path: '/cart',
		element: <Cart />
	},
	{
		path: '/product-detail/:productID',
		element: <ProductDetail />
	},
	{
		path: '/user',
		element: <UserIndex />
	},
	{
		path: '/order',
		element: <OrderIndex />
	},
	{
		path: '/submit-success',
		element: <SubmitSuccessIndex />
	},
	{
		path: '/submit-error',
		element: <SubmitErrorIndex />
	},
	{
		path: '/empty-order',
		element: <EmptyOrderIndex />
	},
]