import { createSlice } from "@reduxjs/toolkit";
import { fetchProducts } from "../thunks/product/fetchProducts";
import { fetchSingleProduct } from "../thunks/product/fetchSingleProduct";


const initialState = {
	products: [],
	status: 'idle',
	error: null,
	filterProductsData: [],
	singleProduct: null,
}


const productSlice = createSlice({
	name: 'products',
	initialState,
	reducers: {
		filterProducts: (state, action) => {
			state.filterProductsData = action.payload;
		},
		clearSingleProduct: (state, action) => {
			state.singleProduct = null;
		}
	},
	extraReducers(builder) {
		builder
		// Get All Products
		.addCase(fetchProducts.pending, (state, action) => {
			state.status = 'pending';
		})
		.addCase(fetchProducts.fulfilled, (state, action) => {
			state.status = 'finished';
			state.products = action.payload;
		})
		.addCase(fetchProducts.rejected, (state, action) => {
			state.status = 'failed'
			state.error = action.error.message
		})

		// Get Single Product
		.addCase(fetchSingleProduct.pending, (state, action) => {
			state.status = 'pending';
		})
		.addCase(fetchSingleProduct.fulfilled, (state, action) => {
			state.status = 'finished';
			state.singleProduct = action.payload;
		})
		.addCase(fetchSingleProduct.rejected, (state, action) => {
			state.status = 'failed'
			state.error = action.error.message
		})
	}
})

export const { filterProducts, clearSingleProduct } = productSlice.actions;

export default productSlice.reducer