import { createSlice, nanoid } from "@reduxjs/toolkit";

const orderStorage = JSON.parse(localStorage.getItem('ORDER_DATA')) || []

const initialState = {
	error: null,
	orderData: orderStorage,
}


const orderSlice = createSlice({
	name: 'order',
	initialState,
	reducers: {
		placeOrder: (state, action) => {
			let user = JSON.parse(localStorage.getItem('CURRENT_USER'))
			let { cart, total } = action.payload;
			if(user){
				let { id, username } = user;
				let obj = {
					userID: id,
					userName: username,
					orderID: nanoid(),
					placedOn: new Date().toLocaleString(),
					total,
					products: cart
				}
				state.orderData.push(obj);
			}
		}
	},

})

export const { placeOrder } = orderSlice.actions
export default orderSlice.reducer