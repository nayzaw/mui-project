import { createSlice } from "@reduxjs/toolkit";
import { fetchCategory } from "../thunks/category/fetchCategory";


const initialState = {
	status: 'idle',
	error: null,
	categoryData: [],
	checkedData: []
}


const categorySlice = createSlice({
	name: 'category',
	initialState,
	reducers: {
		changeCategory: (state, action) => {
			let index = action.payload;
			let { checkedData } = state;

			const currentIndex = checkedData.indexOf(index);
			const newChecked = [...checkedData];

			if (currentIndex === -1) {
				newChecked.push(index);
			} else {
				newChecked.splice(currentIndex, 1);
			}
			state.checkedData = newChecked;
		}
	},
	extraReducers(builder) {
		builder
		.addCase(fetchCategory.pending, (state, action) => {
			state.status = 'pending';
		})
		.addCase(fetchCategory.fulfilled, (state, action) => {
			state.status = 'finished';
			state.categoryData = action.payload;
		})
		.addCase(fetchCategory.rejected, (state, action) => {
			state.status = 'failed'
			state.error = action.error.message
		})
	}
})

export const { changeCategory } = categorySlice.actions
export default categorySlice.reducer