import { createSlice, createAsyncThunk } from "@reduxjs/toolkit";
// import { ApiRequest } from '../api/ApiRequest';
// import ApiPath from '../api/ApiPath';

const cartStorage = JSON.parse(localStorage.getItem('CART'))

const initialState = {
	cartData: cartStorage || [],
	status: 'idle',
	error: null,
	subTotal: null,
	total: null,
}

// export const addNewCart = createAsyncThunk('carts/addNewCart', async (data) => {
// 	let obj = {
// 		url: ApiPath.carts,
// 		data,
// 		method: 'post'
// 	}
// 	let response = await ApiRequest(obj);
// 	return response.data;
// })

const cartSlice = createSlice({
	name: 'cart',
	initialState,
	reducers: {
		addToCart: (state, action) => {
			const { current, cart } = action.payload;
			let temp= {};

			const obj = cart.find(i => i.id === current.id);

			if(obj){
				const tempArr = [];
				cart.forEach(e => {
					if(e.id === current.id){
						tempArr.push({ ...e, quantity: e.quantity + 1})
					}else{
						tempArr.push(e);
					}
				});
				state.cartData = tempArr;
			}
			else{
				temp = {...current, quantity: 1}
				state.cartData = [...state.cartData, temp];
			}
			
		},

		removeFromCart: (state, action) => {
			const { current, cart } = action.payload;
			state.cartData = cart.filter(i => i.id !== current.id);
		},

		increment: (state, action) => {
			const { current, cart } = action.payload;
			const tempArr = [];

			cart.forEach(e => {
				if (e.id === current.id) {
					tempArr.push({ ...current, quantity: current.quantity + 1 })
				} else {
					tempArr.push(e);
				}
			});
			state.cartData = tempArr;
		},
		
		decrement: (state, action) => {
			const { current, cart } = action.payload;
			const tempArr = [];

			cart.forEach(e => {
				if (e.id === current.id) {
					let qty;
					current.quantity > 1 ? qty = current.quantity - 1 : qty = 1;
					tempArr.push({ ...current, quantity:  qty})
				} else {
					tempArr.push(e);
				}
			});
			state.cartData = tempArr;
		},

		calculateTotal: (state, action) => {
			const { cart } = action.payload;
			let subtotal = 0, total, tax = 0.05;

			cart.forEach(e => {
				subtotal += e.price * e.quantity;
			});

			total = subtotal + (subtotal * tax);

			state.subTotal = subtotal.toFixed(2);
			state.total = total.toFixed(2);
		}
	},
	// extraReducers(builder) {
	// 	builder
	// 	.addCase(addNewCart.pending, (state, action) => {
	// 		state.status = 'pending';
	// 	})
	// 	.addCase(addNewCart.fulfilled, (state, action) => {
	// 		console.log('action.payload', action.payload)
	// 		state.status = 'finished';
	// 		state.cartData = action.payload;
	// 	})
	// 	.addCase(addNewCart.rejected, (state, action) => {
	// 		state.status = 'failed'
	// 		state.error = action.error.message
	// 	})
	// }
})

export const { addToCart, removeFromCart, increment, decrement, calculateTotal } = cartSlice.actions
export default cartSlice.reducer