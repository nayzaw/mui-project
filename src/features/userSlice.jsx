import { createSlice } from "@reduxjs/toolkit";
import { fetchAllUser } from "../thunks/user/fetchAllUser";
import { fetchSingleUser } from "../thunks/user/fetchSingleUser";
import { loginUser } from "../thunks/user/loginUser";

const sessionToken = localStorage.getItem('TOKEN') || '';

const initialState = {
	status: 'idle',
	error: null,
	userData: [],
	userEmail: [],
	token: sessionToken,
	currentUser: null,
}

const userSlice = createSlice({
	name: 'user',
	initialState,
	reducers: {
	},
	extraReducers(builder) {
		builder
		// fetchAllUser
		.addCase(fetchAllUser.pending, (state, action) => {
			state.status = 'pending';
		})
		.addCase(fetchAllUser.fulfilled, (state, action) => {
			const response = action.payload;
			let temp = []
			state.status = 'finished';

			response.forEach((item) => {
				temp.push(item.email);
			});
			
			state.userEmail = temp;
			state.userData = response;
		})
		.addCase(fetchAllUser.rejected, (state, action) => {
			state.status = 'failed'
			state.error = action.error.message
		})


		// fetchSingleUser
		.addCase(fetchSingleUser.pending, (state, action) => {
			state.status = 'pending';
		})
		.addCase(fetchSingleUser.fulfilled, (state, action) => {
			state.status = 'finished';
			state.currentUser = action.payload;
		})
		.addCase(fetchSingleUser.rejected, (state, action) => {
			state.status = 'failed'
			state.error = action.error.message
		})


		// loginUser
		.addCase(loginUser.pending, (state, action) => {
			state.status = 'pending';
		})
		.addCase(loginUser.fulfilled, (state, action) => {
			state.status = 'finished';
			const {token} = action.payload;
			localStorage.setItem('TOKEN', token);
			state.token = token;
		})
		.addCase(loginUser.rejected, (state, action) => {
			state.status = 'failed'
			state.error = action.error.message
		})
	}
})

export const {  } = userSlice.actions
export default userSlice.reducer